from datetime import datetime
from typing import List
from urllib.parse import unquote

import pyarrow as pa
import pytz
import s3fs

import pandas as pd
from pyarrow.parquet import write_table


def lambda_handler(s3_event, context):
    bucket = s3_event.get("Records")[0].get("s3").get("bucket").get("name")
    object = s3_event.get("Records")[0].get("s3").get("object").get("key")
    path = "s3://" + bucket + "/" + object
    destination_path = "s3://" + bucket + "/job_offers/processed"
    print(path, destination_path)
    file_name = object.split("/")[2]
    job_offers_df: pd.DataFrame = pd.read_csv(path, header=0, sep=",")
    job_offers_df = process_job_offers(job_offers_df)
    job_offers_df = enhance_date_and_create_date_partition(job_offers_df)

    partition_cols = ["city", "date"]
    write_parquet_in_local_or_s3(partition_cols, destination_path, job_offers_df, s3fs.S3FileSystem(), file_name)
    print(job_offers_df.to_string())


def process_job_offers(job_offers: pd.DataFrame) -> pd.DataFrame:
    def city_formatter(city: str):
        try:
            return city.replace(" ", "-").lower()
        except:
            return city

    job_offers["city"] = job_offers["city"].apply(city_formatter)
    return job_offers


def enhance_date_and_create_date_partition(job_offers: pd.DataFrame) -> pd.DataFrame:
    timestamp_format = "%Y-%m-%d %H:%M:%S"

    def epoch(raw_date: str) -> float:
        raw_date_without_ms = raw_date.split(".")[0]
        date = datetime.strptime(raw_date_without_ms, timestamp_format)
        tz_paris = pytz.timezone("Europe/Paris")
        localized_date: datetime = tz_paris.localize(date)
        return localized_date.timestamp()

    def date_partition(raw_date: str) -> str:
        return raw_date.split(" ")[0]

    job_offers["timestamp"] = job_offers["created_date"].apply(epoch)
    job_offers["date"] = job_offers["created_date"].apply(date_partition)
    return job_offers


def write_parquet_in_local_or_s3(partition_cols, path, dataframe, file_system, file_name, type=None):
    write_parquet_in_s3(dataframe, partition_cols, unquote(path), unquote(file_name))


def write_parquet_in_s3(df: pd.DataFrame, partition_cols: List[str], s3_path: str, file_name: str):
    s3_fs = s3fs.S3FileSystem()

    if partition_cols is None or partition_cols == []:
        raise ValueError("Partition columns can not be empty")
    partition_keys = [df[col] for col in partition_cols]
    data_df = df.drop(partition_cols, axis="columns")
    data_cols = df.columns.drop(partition_cols)
    if len(data_cols) == 0:
        raise ValueError("No data left to save outside partition columns")

    for keys, sub_group in data_df.groupby(partition_keys):
        sub_table = pa.Table.from_pandas(df=sub_group, preserve_index=False)
        full_path = generate_s3_full_path(s3_path, keys, partition_cols, file_name)

        with s3_fs.open(full_path, "wb") as file:
            write_table(sub_table, file)


def generate_s3_full_path(s3_path, keys, partition_cols, file_name) -> str:
    if not isinstance(keys, tuple):
        keys = (keys,)
    sub_dir = ["{colname}={value}".format(colname=name, value=val) for name, val in zip(partition_cols, keys)]
    parquet_file_name = file_name + ".parquet"
    return "/".join([s3_path, *sub_dir, parquet_file_name])


